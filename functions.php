<?php

function restu_function(){


	//adding navwalker file
	require_once get_template_directory(). '/wp-bootstrap-navwalker.php';
	//custom title
	add_theme_support('title-tag');

	//custom background
	add_theme_support('custom-background');

	//thumbnail
	add_theme_support('post-thumbnail');

	//custom logo
	add_theme_support('custom-logo', array(
		'defult-image'	=> get_template_directory_uri().'/img/logo.png',
	));

	//load theme textdomain
	load_theme_textdomain('restu',get_template_directory_uri().'/language');

	//register menu
if(function_exists('register_nav_menus')){
	register_nav_menus(array(
	'primarymenu' 		=>__('Header Menu','restu'),
	'secondarymenu'		=>__('Footer Menu','restu')
		));
	}

	//read more
	function read_more(){
		$post_content = explode(" ",get_the_content());
		$less_content = array_slice($post_content, 0, $limit);
		echo implode(" ",$less_content);

	}
}
add_action('after_setup_theme','restu_function');






function restu_widgets(){

	 register_sidebar(array(
		'name' => __('Footer Widgets','restu'),
		'description' => __('Add your footer widgets here','restu'),
		'id' => 'footer-widget',
		'before_widget' => '<div class="col-lg-4"><div class="footer-widget pr-lg-5 pr-0">',
		'after_widget' => '</p></div></div>',
		'before_title' => '<h4>',
		'after_title' => '<p></h4>'

	));


}
add_action('widgets_init','restu_widgets');

?>